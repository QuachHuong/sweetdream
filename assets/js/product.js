var menu = {
    init: function() {
        this.clickShowMenu('#clickbar', '#menubar', 'active');
        this.clickShowMenu('#clickbar1', '#menubar1', 'active');
        this.clickShowMenu('#clickbar2', '#menubar2', 'active');
        this.clickShowMenu('#clickbar3', '#menubar3', 'active');
        this.clickShowMenu('#clickbar4', '#menubar4', 'active');
        this.clickShowMenu('#clickbar5', '#menubar5', 'active');
        this.clickShowMenu('#clickbar6', '#menubar6', 'active');
        this.clickShowMenu('#clickbar7', '#menubar7', 'active');

    },
    clickShowMenu: function(button, header, classMenu) {
        var btn = document.querySelector(button);
        var navMenu = document.querySelector(header);
        btn.addEventListener('click', function() {
            navMenu.classList.toggle(classMenu);
            btn.classList.toggle('active1');
        });
    }
}
menu.init();